import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: '25ch',
    },
  }));  

function Form(){

    const [libro, setLibro] = useState(
            {
                title: "",
                author: "",
                isbn: ""
            }
        );

    const handleChange = (e) => {
        setLibro(state => ({
            ...state,
            [e.target.id] : e.target.value
        }));
    }

    const handleData = () => {
        console.log(libro);
        setLibro(state => ({
            ...state,
            title: "",
            author: "",
            isbn: ""
        }));
    }

    const classes = useStyles();

    return(
        
            <form>
                <div className={classes.root}>
                    <div>
                        <TextField
                            id="title"
                            label="Title"
                            style={{ margin: 8 }}
                            placeholder="Title"
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                            value={libro.title}
                            onChange={handleChange}
                        />
                        <TextField
                            id="author"
                            label="Author"
                            style={{ margin: 8 }}
                            placeholder="Author"
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                            value={libro.author}
                            onChange={handleChange}
                        />
                        <TextField
                            id="isbn"
                            label="ISBN"
                            style={{ margin: 8 }}
                            placeholder="ISBN"
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                            value={libro.isbn}
                            onChange={handleChange}
                        />
                        <Button variant="contained" color="primary" size="large" onClick={handleData}>
                            Submit
                        </Button>
                    </div>
                </div>
            </form>
    )
}

export default Form;